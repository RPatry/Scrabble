##### Functions

#map, called like this: $(map myfunc, mylist)
map = $(foreach t, $(2), $(call $(1),$(t)))

#map_1, called like this: $(map myfunc, mylist, param) where myfunc takes two parameters: the first is param, the second an item from mylist.
map_1 = $(foreach t, $(2), $(call $(1),$(3),$(t)))

##### The Makefile itself

SRCDIR := src
INCDIR := include
#g++ has to be >= 4.9, so as to avoid warnings when initialising member const references with another const reference in a constructor.
CXX := g++
INC := -I. -I/home/raphael/Programming/Codeblocks_Projects/SDL++/ -I/usr/local/include/fmodex/ -I./$(INCDIR)
LDLIBS := -lfmod /home/raphael/Programming/Codeblocks_Projects/SDL++/bin/Release/libSDL++.a -lSDL2 -lSDL2_image -lSDL2_ttf
CXXFLAGS := -std=c++11 -Wall -Wextra -Werror -pedantic -save-temps=obj $(INC)
RFLAGS := -O2
DFLAGS := -g
SRCSUBDIRS := $(SRCDIR) $(shell find $(SRCDIR) -type d)
DEPSDIR := .deps
DEPSDIRS := $(DEPSDIR) $(call map_1, addprefix, $(SRCSUBDIRS), .deps/Debug/) $(call map_1, addprefix, $(SRCSUBDIRS), .deps/Release/)
OBJDIRS := Obj $(call map_1, addprefix, $(SRCSUBDIRS), Obj/Debug/) $(call map_1, addprefix, $(SRCSUBDIRS), Obj/Release/)
BINDIRS := Bin Bin/Debug Bin/Release

EXEC := DictServer

SRC := $(shell find $(SRCDIR) -type f -name "*.cpp")
ASM := $(shell find $(SRCDIR) -type f -name "*.s")
DOBJS := $(SRC:$(SRCDIR)/%.cpp=Obj/Debug/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=Obj/Debug/$(SRCDIR)/%.o)
ROBJS := $(SRC:$(SRCDIR)/%.cpp=Obj/Release/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=Obj/Release/$(SRCDIR)/%.o)
OBJS := $(DOBJS) $(ROBJS)
DDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/Debug/$(SRCDIR)/%.cpp.d)
RDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/Release/$(SRCDIR)/%.cpp.d)
DEPS := $(DDEPS) $(RDEPS)
OUTASM := $(SRC:$(SRCDIR)/%.cpp=Obj/Release/%.s) $(SRC:%.cpp=Obj/Debug/%.s)
II := $(SRC:%.cpp=Obj/Release/%.ii) $(SRC:%.cpp=Obj/Debug/%.ii)

.PHONY: all debug release
all: setup debug release

-include $(DEPS)

debug: Bin/Debug/$(EXEC)
release: Bin/Release/$(EXEC)

Bin/Debug/$(EXEC): $(DOBJS)
	$(CXX) -o $@ $^ $(LDLIBS)

Bin/Release/$(EXEC): $(ROBJS)
	$(CXX) -o $@ $^ -s $(LDLIBS)

Obj/Debug/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/Debug/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

Obj/Release/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/Release/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

Obj/Debug/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

Obj/Release/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

.PHONY: setup
setup: 
	mkdir -p $(DEPSDIRS) $(BINDIRS) $(OBJDIRS)
	mkdir -p Debug/Bin Release/Bin Debug/Obj Release/Obj Depend 2> /dev/null

.PHONY: clean
clean:
	rm -f $(OBJS) Debug/Bin/$(EXEC) Release/Bin/$(EXEC) $(OUTASM) $(II) $(DEPS)
