#include "boost/test/unit_test.hpp"
#include "algo.hpp"
#include <vector>

#define BOOST_TEST_DYM_LINK
#define BOOST_TEST_MODULE Algo

using namespace std;
using namespace algo;

//check_duplicates test cases
BOOST_AUTO_TEST_SUITE(algo_check_duplicates_suite)

BOOST_AUTO_TEST_CASE(check_duplicates_with_no_duplicates)
{
    const vector<int> vals { 4, 48, 5, 12, 3 };
    BOOST_CHECK(!check_duplicates(vals));
}

BOOST_AUTO_TEST_CASE(check_duplicates_with_duplicates)
{
    const vector<int> vals { 4, 48, 5, 4, 12, 3 };
    BOOST_CHECK(check_duplicates(vals));
    const vector<int> vals2 { 48, 12, 5, 12, 3, 48 };
    BOOST_CHECK(check_duplicates(vals2));
}

BOOST_AUTO_TEST_CASE(check_duplicates_empty_list)
{
    const vector<int> empty = {};
    BOOST_CHECK(!check_duplicates(empty));
}

BOOST_AUTO_TEST_SUITE_END()

//transform
BOOST_AUTO_TEST_SUITE(algo_transform_suite)

BOOST_AUTO_TEST_CASE(transform_normal_list)
{
  const std::vector<int> values { 5, 9, 4, 11, 3, 6 };
  const std::vector<bool> value_is_over_seven { algo::transform(values.begin(), values.end(), [](const int val) {
      return val > 7;
  }) };
  const std::vector<bool> expected { false, true, false, true, false, false };
  BOOST_TEST(value_is_over_seven == expected, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END()

//is_in_range
BOOST_AUTO_TEST_SUITE(algo_is_in_range_suite)

BOOST_AUTO_TEST_CASE(is_in_range_inside_range_integer)
{
    const int low = 5, high = 20;
    for (int i = low; i < high; ++i)
    {
	BOOST_CHECK(is_in_range(low, high, i));
    }
}

BOOST_AUTO_TEST_CASE(is_in_range_outside_range_integer)
{
    BOOST_CHECK(!is_in_range(5, 20, 4));
    BOOST_CHECK(!is_in_range(5, 20, 21));
    BOOST_CHECK(!is_in_range(5, 20, 42));
    BOOST_CHECK(!is_in_range(5, 20, 0));
}

BOOST_AUTO_TEST_CASE(is_in_range_one_element_in_range)
{
    BOOST_CHECK(is_in_range(5, 5, 5));
}

BOOST_AUTO_TEST_SUITE_END()
