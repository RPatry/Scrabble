#include "boost/test/unit_test.hpp"
#include "json_messages.hpp"
#include "game_rules.hpp"
#include "grid_state.hpp"
#include "json.hpp"
#include <iostream>
#include <algorithm>
#include <type_traits>

#define BOOST_TEST_DYM_LINK
#define BOOST_TEST_MODULE JsonMessages

using namespace scrabble::network::messages;
using namespace scrabble::game;
using namespace std;

using json_type = nlohmann::json;

/* Contrary to my usual use of {} to initialize objects, I use parentheses in this module,
as, much like vector, json_type accepts an initializer_list as a constructor argument,
which of course takes precedence over copy initialization
So yeah. It has been the cause of quite a few headaches too.
*/

/*! Automate equal assertions when working with json objects */
template<typename T>
void boost_check_json_equal(json_type const& js, T const& t)
{
    BOOST_CHECK_EQUAL(js, json_type(t));
}

// This function only exists if T is not a json_type: otherwise there would be an ambiguity with the other template above with the parameters inversed
template<typename T>
typename std::enable_if<!std::is_same<T, json_type>::value, void>::value
	       boost_check_json_equal(T const& t, json_type const& js)
{
    boost_check_json_equal(js, t);
}

void boost_check_json_equal(json_type const& js, char ch)
{
    boost_check_json_equal(js, string{ch});
}

void boost_check_json_equal(char ch, json_type const& js)
{
    boost_check_json_equal(js, string{ch});
}

BOOST_AUTO_TEST_SUITE(message_sending_suite)

BOOST_AUTO_TEST_CASE(make_allnames_message_check_contents)
{
    const vector<string> names { "Emily", "Christian", "Ewan" };
    json_type expected = {"allnames"};
    for (const auto& name : names)
    {
	expected.push_back(name);
    }
    auto actual_string = make_allnames_message(names);
    auto actual = json_type::parse(actual_string.c_str());
    BOOST_CHECK_EQUAL(actual, expected);
}

BOOST_AUTO_TEST_CASE(gridbonus_to_string_correct_outputs)
{
    using namespace scrabble::game;
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::None), string{"E"});
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::Game_Begin), string{"B"});
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::Double_Letter), string{"L2"});
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::Triple_Letter), string{"L3"});
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::Double_Word), string{"W2"});
    BOOST_CHECK_EQUAL(gridbonus_to_string(Grid_Tile_Bonus::Triple_Word), string{"W3"});
}

BOOST_AUTO_TEST_CASE(make_gridbonuses_message_check_contents)
{
    using namespace scrabble::game;
    const std::vector<std::vector<Grid_Tile_Bonus>> grid { { Grid_Tile_Bonus::None, Grid_Tile_Bonus::Game_Begin, Grid_Tile_Bonus::Double_Letter },
							     { Grid_Tile_Bonus::None, Grid_Tile_Bonus::Double_Word, Grid_Tile_Bonus::Triple_Letter },
							     { Grid_Tile_Bonus::Triple_Word, Grid_Tile_Bonus::None, Grid_Tile_Bonus::Double_Letter  } };
    const auto actual_string = make_gridbonuses_message(grid);
    const auto actual = json_type::parse(actual_string.c_str());

    json_type expected = {"gridbonuses"}, parameter = {};
    for (const auto& row : grid)
    {
	json_type array_row = {};
	for (const auto& bonus : row)
	{
	    array_row.push_back(gridbonus_to_string(bonus));
	}
	parameter.push_back(array_row);
    }
    expected.push_back(parameter);
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(make_tilespicked_message_check_contents)
{
    vector<char> tiles { 'z', '_', 's', 'h' };
    json_type expected = {"tilespicked"};
    for (const char tile : tiles)
    {
	expected.push_back(string{tile});
    }
    const auto actual_string = make_tilespicked_message(tiles);
    const auto actual = json_type::parse(actual_string.c_str());
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(make_playersorder_message_check_contents)
{
    json_type expected = {"playersorder"};
    vector<unsigned int> order { 3, 1, 2, 4 };
    for (const auto rank : order)
    {
	expected.push_back(rank);
    }
    const auto actual_string = make_playersorder_message(order);
    const auto actual = json_type::parse(actual_string.c_str());
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(make_attemptby_message_check_contents)
{
    const Player_Index index{2};
    const bool move_valid = true;
    using Pos = Ranged_uint<0, Rules::Grid_Width - 1>;
    const Tiles_with_Positions twp { { Grid_Pos{Pos{1}, Pos{2}}, Grid_Tile{'a', false} },
	{Grid_Pos{Pos{6}, Pos{4}}, Grid_Tile{'s', true}} };
    const auto message = make_attemptby_message(index, move_valid, twp);
    cout << message << endl;
    json_type js = json_type::parse(message.c_str());
    BOOST_CHECK_EQUAL(js.size(), 4U);
    boost_check_json_equal(js[0], string{"attemptby"});
    boost_check_json_equal(js[1], static_cast<unsigned int>(index));
    boost_check_json_equal(js[2], move_valid);
    const auto list_tiles_pos = js[3];
    BOOST_CHECK_EQUAL(list_tiles_pos.size(), 2U);
    const auto first = list_tiles_pos[0];
    const auto second = list_tiles_pos[1];
    BOOST_CHECK_EQUAL(first.size(), 3U);
    BOOST_CHECK_EQUAL(second.size(), 4U);

    boost_check_json_equal(first[0], twp[0].first.x.get());
    boost_check_json_equal(first[1], twp[0].first.y.get());
    boost_check_json_equal(first[2], twp[0].second.letter);

    boost_check_json_equal(second[0], twp[1].first.x.get());
    boost_check_json_equal(second[1], twp[1].first.y.get());
    boost_check_json_equal(second[2], blank_tile);
    boost_check_json_equal(second[3], twp[1].second.letter);
}

BOOST_AUTO_TEST_CASE(make_yourresults_message_check_contents_move_valid)
{
    const List_of_Words_with_Points list_wp { {"tree", 19}, {"sun", 7}, {"crooked", 25}, {"account", 21}, {"impressive", 32}, {"nimble", 26}, {"centipede", 27} };
    const vector<string> new_letters { "a", "s", "z", "b", "j" };
    const string message { make_yourresults_message(true, new_letters, list_wp) };
    cout << message << endl;
    json_type js = json_type::parse(message.c_str());
    BOOST_CHECK_EQUAL(js.size(), 4U);
    boost_check_json_equal(js[0], "yourresults");
    boost_check_json_equal(js[1], true);

    json_type array_word_points = js[2];
    BOOST_CHECK_EQUAL(array_word_points.size(), list_wp.size());
    for (const auto& json_pair : array_word_points)
    {
	BOOST_CHECK_EQUAL(json_pair.size(), 2U);  //two elements, the word and its score.
	/*Can we find this array in our original data structure?
	Note we make no assumptions about the ordering in the final json object
	compared to the original ordering in our data structure. */
	const auto iter = find_if(list_wp.begin(), list_wp.end(), [&](Word_with_Points const& pair_wp) {
		return json_pair[0] == json_type(pair_wp.first) && json_pair[1] == json_type(pair_wp.second);
	});
	//Check find_if did not stop at the end of the structure => the element was found
	BOOST_CHECK_MESSAGE(iter != list_wp.end(), "json pair: " << json_pair);
    }

    /*Finally, we also check the new letters for the player for the turn.
      Again, we don't care about the ordering, only that all letters can be found. */
    json_type array_of_new_letters = js[3];
    for (const auto& new_letter : array_of_new_letters)
    {
	BOOST_CHECK_EQUAL(new_letter.size(), 1U);
	const auto iter = find_if(new_letters.begin(), new_letters.end(), [&](string const& s) {
		return json_type(s) == new_letter;
	});
	//Was the letter found?
	BOOST_CHECK_MESSAGE(new_letters.end() != iter, "letter: " << new_letter);
    }
}

BOOST_AUTO_TEST_CASE(make_yourresults_message_check_contents_move_invalid)
{
    //Much faster, only two portions of the json object to check.
    const string message { make_yourresults_message(false) };
    cout << endl << message << endl;

    json_type js = json_type::parse(message.c_str());
    BOOST_CHECK_EQUAL(js.size(), 2U);
    boost_check_json_equal(js[0], string{"yourresults"});
    boost_check_json_equal(js[1], false);
}

BOOST_AUTO_TEST_CASE(make_putwords_message_check_correct_contents)
{
    const List_of_Words_with_Points list_wp { {"tree", 19}, {"sun", 7}, {"crooked", 25}, {"account", 21}, {"impressive", 32}, {"nimble", 26}, {"centipede", 27} };
    const Player_Index player { 2 };
    const string message { make_putwords_message(player, list_wp) };
    cout << endl << message << endl;

    json_type js = json_type::parse(message.c_str());
    BOOST_CHECK_EQUAL(js.size(), 3U);
    boost_check_json_equal(js[0], string{"putwords"});
    boost_check_json_equal(js[1], player.get());
    json_type array_words = js[2];
    for (const auto& word_point : array_words)
    {
	BOOST_CHECK_EQUAL(word_point.size(), 2U);
	//We check if list_wp and array_words are equivalent, that is, if each element of the second can be found in the first.
	//We ignore the ordering of the elements, explaining the find_if.
	const auto iter = find_if(list_wp.begin(), list_wp.end(), [&word_point](const Word_with_Points wp) {
		return word_point[0] == json_type(wp.first) && word_point[1] == json_type(wp.second);
	});
	BOOST_CHECK_MESSAGE(iter != list_wp.end(), "array: " << word_point);
    }

}

BOOST_AUTO_TEST_CASE(make_givesup_message_check_correct_contents)
{
    const Player_Index player { 3 };
    const string message { make_givesup_message(player) };
    cout << endl << message << endl;
    json_type js = json_type::parse(message.c_str());
    BOOST_CHECK_EQUAL(js.size(), 2U);
    boost_check_json_equal(js[0], string{"givesup"});
    boost_check_json_equal(js[1], player.get());
}

BOOST_AUTO_TEST_CASE(make_you_message_check_correct_contents)
{
    const string message { make_you_message() };
    json_type js = json_type::parse(message.c_str());
    cout << endl << message << endl;
    BOOST_CHECK_EQUAL(js.size(), 1U);
    boost_check_json_equal(js[0], string{"you"});
}

BOOST_AUTO_TEST_CASE(make_lolno_message_check_correct_contents)
{
    const string message { make_lolno_message() };
    json_type js = json_type::parse(message.c_str());
    cout << endl << message << endl;
    BOOST_CHECK_EQUAL(js.size(), 1U);
    boost_check_json_equal(js[0], string{"lolno"});
}

BOOST_AUTO_TEST_CASE(make_what_message_check_correct_contents)
{
    const string message { make_what_message() };
    json_type js = json_type::parse(message.c_str());
    cout << endl << message << endl;
    BOOST_CHECK_EQUAL(js.size(), 1U);
    boost_check_json_equal(js[0], string{"what"});
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_CASE(parse_message_type_check_all_outputs_valid)
{
    const vector<string> messages { "myattempt", "skip", "myname", "foryou",
	    "qfsd\", ,qqqqq, '''', \"" };   //Yeah, pretty bad, creating a JSON injection in my own code... (see below)
    const vector<Received_Messages> expected { Received_Messages::My_Attempt, Received_Messages::Skip, Received_Messages::My_Name, Received_Messages::Unknown, Received_Messages::Unknown };
    for (size_t i = 0; i < messages.size(); ++i)
    {
	const string actual_message = "[\"" + messages.at(i) + "\"]";
	json_type js = { actual_message };
	const Received_Messages type { parse_message_type(js[0]) };
	BOOST_CHECK_EQUAL(type, expected.at(i));
    }
}

BOOST_AUTO_TEST_CASE(check_skip_message_correct_correct_output)
{
    json_type js = {"skip"};
    const string still_correct = js.dump(0);
    BOOST_CHECK(check_skip_message_correct(still_correct));
    js.push_back("a useless parameter");
    const string now_incorrect = js.dump(0);
    BOOST_CHECK(!check_skip_message_correct(now_incorrect));
    //Empty strings don't work, either
    BOOST_CHECK(!check_skip_message_correct(""));
    //Bad spelling => not working
    json_type bad_spelling_js = {"skeep"};
    const string bad_spelling = bad_spelling_js.dump(0);
    BOOST_CHECK(!check_skip_message_correct(bad_spelling));
}

BOOST_AUTO_TEST_SUITE(parse_myattempt_message_suite)

BOOST_AUTO_TEST_CASE(parse_myattempt_message_correct_output)
{
    //c is a real letter for the first tile. The second tile is a blank letter, but the player intended to use the z letter in its stead.
    auto js = R"(["myattempt",
[5, 4, "c"],
[8, 6, "_", "z" ] ])"_json;
    const auto opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(opt.is_initialized());
    const Tiles_with_Positions& actual = opt.value();
    const Tiles_with_Positions expected {
	Tile_with_Position{ Grid_Pos{Ranged_uint<0, Rules::Grid_Width - 1>{5}, Ranged_uint<0, Rules::Grid_Height - 1>{4}}, Grid_Tile{'c', false}},
	Tile_with_Position{ Grid_Pos{Ranged_uint<0, Rules::Grid_Width - 1>{8}, Ranged_uint<0, Rules::Grid_Height - 1>{6}}, Grid_Tile{'z', true}},
	    };
    BOOST_CHECK_EQUAL_COLLECTIONS(expected.begin(), expected.end(), actual.begin(), actual.end());
}

BOOST_AUTO_TEST_CASE(parse_myattempt_message_incorrect_spelling)
{
    json_type js = R"(["maiattampt",
[5, 5, "a"] ])"_json;
    const auto opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_CASE(parse_myattempt_message_incorrect_positions)
{
    json_type js = R"(["myattempt",
[16, 2, "b"], [12, 11, "h"]])"_json;
    auto opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());

    js = R"(["myattempt",
[6, 22, "r"], [2, 10, "s"]])"_json;
    opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_CASE(parse_myattempt_message_incorrect_tiles)
{
    //Blank tile without intended letter.
    auto js = R"(["myattempt",
[5, 4, "_"],
[8, 6, "l"] ])"_json;
    auto opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());

    //A letter with another letter, as though it was a blank tile.
    js = R"(["myattempt",
[5, 4, "s"],
[8, 6, "l", "j"] ])"_json;
    opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());

    //A blank tile with another blank tile as a letter.
    js = R"(["myattempt",
[5, 4, "_", "_"],
[8, 6, "l"] ])"_json;
    opt = parse_myattempt_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());

}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(parse_myname_message_suite)

BOOST_AUTO_TEST_CASE(parse_myname_message_valid)
{
    auto js = R"(
["myname", "Hector"])"_json;
    const auto opt = parse_myname_message(js.dump(0));
    BOOST_REQUIRE(opt.is_initialized());
    BOOST_CHECK_EQUAL(opt.value(), string{"Hector"});
}

BOOST_AUTO_TEST_CASE(parse_myname_message_too_many_arguments)
{
    auto js = R"(
["myname", "Chris", "Mike", "Victor"])"_json;
    auto opt = parse_myname_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_CASE(parse_myname_message_no_arguments)
{
    auto js = R"(["myname"])"_json;
    const auto opt = parse_myname_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_CASE(parse_myname_message_bad_spelling)
{
    auto js = R"(["miname", "William"])"_json;
    const auto opt = parse_myname_message(js.dump(0));
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_CASE(parse_myname_message_empty_message)
{
    const auto opt = parse_myname_message("");
    BOOST_CHECK(!opt.is_initialized());
}

BOOST_AUTO_TEST_SUITE_END()
