#include <string>
#include "boost/test/unit_test.hpp"
#include "tile.hpp"

#define BOOST_TEST_DYM_LINK
#define BOOST_TEST_MODULE Tile

using namespace scrabble::game;

static const std::string alphabet {"abcdefghijklmnopqrstuvwxyz"};

BOOST_AUTO_TEST_CASE(alphabet_and_blank_are_valid_tiles)
{
    for (const char letter : alphabet)
    {
	BOOST_CHECK(letter_is_tile(letter));
    }
    BOOST_CHECK(letter_is_tile(blank_tile));
}

BOOST_AUTO_TEST_CASE(invalid_letters_not_valid_tiles)
{
    //These are just a few samples of invalid tiles.
    //We can't really try them all though.
    BOOST_CHECK(!letter_is_tile('4'));
    BOOST_CHECK(!letter_is_tile(';'));
    BOOST_CHECK(!letter_is_tile('$'));
    BOOST_CHECK(!letter_is_tile('*'));
    BOOST_CHECK(!letter_is_tile('%'));
}

BOOST_AUTO_TEST_CASE(alphabet_and_blank_can_be_built_as_tiles)
{
    for (const char letter : alphabet)
    {
	BOOST_REQUIRE_NO_THROW(Tile{letter});
	const Tile tile{letter};   //No exception should be thrown.
	BOOST_CHECK_EQUAL(letter, tile.letter);  //While we're at it...
    }
    BOOST_REQUIRE_NO_THROW(Tile{blank_tile});
    const Tile tile_blank_letter{blank_tile};
    BOOST_CHECK_EQUAL(blank_tile, tile_blank_letter.letter);  //While we're at it...
}

BOOST_AUTO_TEST_CASE(invalid_letters_cannot_be_built_as_tiles)
{
    BOOST_CHECK_THROW(Tile{'4'}, Invalid_Tile);
    BOOST_CHECK_THROW(Tile{';'}, Invalid_Tile);
    BOOST_CHECK_THROW(Tile{'$'}, Invalid_Tile);
    BOOST_CHECK_THROW(Tile{'*'}, Invalid_Tile);
    BOOST_CHECK_THROW(Tile{'%'}, Invalid_Tile);
}

