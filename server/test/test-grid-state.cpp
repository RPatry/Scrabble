#include "boost/test/unit_test.hpp"
#include "grid_state.hpp"

#define BOOST_TEST_DYM_LINK
#define BOOST_TEST_MODULE GridState

static const std::string alphabet {"abcdefghijklmnopqrstuvwxyz"};

using namespace scrabble::game;

BOOST_AUTO_TEST_CASE(grid_tile_exceptions_appropriate)
{
    for (const char letter : alphabet)
    {
	BOOST_CHECK_NO_THROW((Grid_Tile{letter, true}));
	BOOST_CHECK_NO_THROW((Grid_Tile{letter, false}));
    }
    BOOST_CHECK_THROW((Grid_Tile{'4', false}), Not_in_alphabet);
}

Grid_Pos make_pos(unsigned int x, unsigned int y)
{
    return Grid_Pos{Ranged_uint<0, 14>{x},  Ranged_uint<0, 14>{y}};
}

BOOST_AUTO_TEST_CASE(check_no_duplicate_position_with_no_duplicates)
{
    std::vector<Grid_Pos> positions_no_dups { make_pos(5, 8), make_pos(7, 4), make_pos(12, 5), make_pos(0, 0), make_pos(7, 3) };
    BOOST_CHECK(check_no_duplicate_position(positions_no_dups));
}

BOOST_AUTO_TEST_CASE(check_no_duplicate_position_with_duplicates)
{
    std::vector<Grid_Pos> positions_dups { make_pos(5, 8), make_pos(7, 4), make_pos(12, 5), make_pos(0, 0), make_pos(7, 3), make_pos(7, 4) };
    BOOST_CHECK(!check_no_duplicate_position(positions_dups));
}

BOOST_AUTO_TEST_CASE(grid_state_can_be_built)
{
    BOOST_CHECK_NO_THROW(Grid_State{});
}

std::vector<Optional<Grid_Tile>> make_grid_tiles(void) noexcept
{
    std::vector<Optional<Grid_Tile>> result{Rules::Grid_Width * Rules::Grid_Height};
    return result;
}

BOOST_AUTO_TEST_CASE(check_all_grid_tiles_empty_detect_non_empty)
{
    auto tiles = make_grid_tiles();
    tiles[2].emplace('c', false);
    tiles[15].emplace('f', false);
    const Grid_State state{tiles};
    std::vector<Grid_Pos> positions = {make_pos(2, 0)}, positions2 = {make_pos(0, 1)}, positions3{make_pos(4, 6), make_pos(3, 9)}, positions4 = {make_pos(2, 0), make_pos(0, 1)};
    BOOST_CHECK((!check_all_grid_tiles_empty(state, positions)));
    BOOST_CHECK((!check_all_grid_tiles_empty(state, positions2)));
    BOOST_CHECK((!check_all_grid_tiles_empty(state, positions4)));
    BOOST_CHECK((check_all_grid_tiles_empty(state, positions3)));
}

using Pair = std::pair<Grid_Pos, Grid_Tile>;

BOOST_AUTO_TEST_CASE(grid_state_add_tiles_duplicates_non_allowed)
{
    std::vector<Pair> positions = {Pair{make_pos(2, 0), Grid_Tile{'l', true}}, Pair{make_pos(2, 0), Grid_Tile{'i', false}}};
    const Grid_State state{};
    auto new_state = state.add_tiles(positions);
    BOOST_CHECK(!new_state.is_initialized());
}

BOOST_AUTO_TEST_CASE(grid_state_add_tiles_non_empty_non_allowed)
{
    auto tiles = make_grid_tiles();
    tiles[2].emplace('c', false);
    tiles[15].emplace('f', false);
    const Grid_State state{tiles};
    std::vector<Pair> tiles_with_pos = { Pair{make_pos(2, 0), Grid_Tile{'l', true}}, Pair{make_pos(0, 1), Grid_Tile{'i', false}} };
    auto new_state = state.add_tiles(tiles_with_pos);
    BOOST_CHECK(!new_state.is_initialized());
}

BOOST_AUTO_TEST_CASE(grid_state_add_tiles_works_if_preconditions_held)
{
    auto tiles = make_grid_tiles();
    tiles[8].emplace('c', false);
    tiles[11].emplace('f', false);
    const Grid_State state{tiles};
    std::vector<Pair> tiles_with_pos = { Pair{make_pos(2, 0), Grid_Tile{'l', true}}, Pair{make_pos(0, 1), Grid_Tile{'i', false}} };
    auto new_state = state.add_tiles(tiles_with_pos);
    BOOST_REQUIRE(new_state.is_initialized());
    std::vector<Grid_Pos> all_positions { make_pos(2, 0), make_pos(0, 1), make_pos(8, 0), make_pos(11, 0) };
    for (const auto& pos : all_positions)
    {
	const auto& tile = (*new_state).get_tile(pos);
	BOOST_CHECK_MESSAGE(tile.is_initialized(), "position: " << pos.x << ", " << pos.y);
    }
}
