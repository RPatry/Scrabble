#include <iostream>
#include <string>
#include "game_rules.hpp"
#include "algo.hpp"
#include "ranged/ranged.hpp"

using namespace std;
using namespace scrabble::game;
using namespace algo;

/*! A type of number that is guaranteed to contain a valid number of players */
using Number_of_Players = Ranged_uint<Rules::Min_Number_Players, Rules::Max_Number_Players>;

/*! Read the number of players for this game from the standard input */
Number_of_Players get_number_of_players(void) noexcept
{
    unsigned int nb_players { 0 };
    while (!is_in_range(Rules::Min_Number_Players, Rules::Max_Number_Players, nb_players))
    {
	cout << "Entrez un nombre de joueurs [" << Rules::Min_Number_Players << '-' << Rules::Max_Number_Players << "]: ";
	cin >> nb_players;
	if (!cin)
	{
	    cin.clear();
	    string dummy{};
	    cin >> dummy;  //Consume the garbage input from the user.
	}
    }

    return Number_of_Players{nb_players};
}

int main(int argc, char** argv)
{
    //Check if the user gave a good argument (path to the dictionary file).
    if (argc != 2)
    {
	cout << "usage: " << argv[0] << " path_to_dictionary.txt" << endl;
	return 1;
    }
    const string path_to_dictionary { argv[1] };
    const auto number_players = get_number_of_players();
    cout << "Début d'une partie avec " << number_players << " joueurs." << endl;
    return 0;
}
