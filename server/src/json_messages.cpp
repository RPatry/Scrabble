#include "grid_state.hpp"
#include "json_messages.hpp"
#include "json.hpp"  //The library
#include "tile.hpp"
#include "algo.hpp"
#include <sstream>

namespace std {

std::ostream& operator<<(std::ostream& os, scrabble::network::messages::Tile_with_Position const& twp) noexcept
{
    return os << '{' << twp.first << ", " << twp.second << '}';
}

}

namespace scrabble {

namespace network {

namespace messages {

/*! The width for indentation in the generated JSON messages */
const int JsonIndentation = 4;

using json_type = nlohmann::json;

std::string make_allnames_message(std::vector<std::string> const& names_players) noexcept
{
    json_type js = {
	"allnames"};
    for (const auto& name : names_players)
    {
	js.push_back(name);
    }
    return js.dump(JsonIndentation);
}

std::string gridbonus_to_string(game::Grid_Tile_Bonus bonus) noexcept
{
    using namespace game;
    switch (bonus)
    {
	case Grid_Tile_Bonus::None:
	{
	    return "E";
	}
	case Grid_Tile_Bonus::Game_Begin:
	{
	    return "B";
	}
	case Grid_Tile_Bonus::Double_Letter:
	{
	    return "L2";
	}
	case Grid_Tile_Bonus::Triple_Letter:
	{
	    return "L3";
	}
	case Grid_Tile_Bonus::Double_Word:
	{
	    return "W2";
	}
	case Grid_Tile_Bonus::Triple_Word:
	{
	    return "W3";
	}
	default:
	{
	    return "?";
	}
    }
}

std::string make_gridbonuses_message(std::vector<std::vector<game::Grid_Tile_Bonus>> const& grid_bonuses) noexcept
{
    json_type js = {
	"gridbonuses"};
    json_type outer_array = {};
    for (const auto& row_of_bonuses : grid_bonuses)
    {
	json_type inner_array = {};
	for (const auto bonus : row_of_bonuses)
	{
	    inner_array.push_back(gridbonus_to_string(bonus));
	}
	outer_array.push_back(inner_array);
    }
    js.push_back(outer_array);
    return js.dump(JsonIndentation);
}

std::string make_tilespicked_message(std::vector<char> tiles) noexcept
{
    json_type js = {
	"tilespicked"};
    for (const char tile : tiles)
    {
	js.push_back(std::string{tile});
    }
    return js.dump(JsonIndentation);
}

std::string make_playersorder_message(std::vector<unsigned int> ordering) noexcept
{
    json_type js = {
	"playersorder"};
    for (const unsigned int rank : ordering)
    {
	js.push_back(rank);
    }
    return js.dump(JsonIndentation);
}

std::string make_attemptby_message(const Player_Index player, const bool move_valid, Tiles_with_Positions const& tiles_with_positions) noexcept
{
    json_type js = {
	"attemptby",
	static_cast<unsigned int>(player),
	move_valid
    };
    json_type array_of_tiles{};
    for (const auto& tile_with_pos : tiles_with_positions)
    {
	const auto& pos = tile_with_pos.first;
	const auto& tile = tile_with_pos.second;
	json_type array_tile = {static_cast<unsigned int>(pos.x), static_cast<unsigned int>(pos.y)};
	if (tile.is_blank)
	{
	    array_tile.push_back(std::string{game::blank_tile});
	}
	array_tile.push_back(std::string{tile.letter});
	array_of_tiles.push_back(array_tile);
    }
    js.push_back(array_of_tiles);
    return js.dump(0);
}

std::string make_yourresults_message(const bool move_valid, std::vector<std::string> const& new_letters, List_of_Words_with_Points const& words_put_on_grid) noexcept
{
    json_type js = {
	"yourresults",
	move_valid
    };
    if (move_valid)
    {
	json_type words_with_points_json_array {};
	for (const auto& word_with_score : words_put_on_grid)
	{
	    const auto& word = word_with_score.first;
	    const auto score = word_with_score.second;
	    json_type array_ws { word, score };
	    words_with_points_json_array.push_back(array_ws);
	}
	//json_type new_letters_json_array { new_letters };
	js.push_back(words_with_points_json_array);
	js.push_back(new_letters);
    }

    return js.dump(JsonIndentation);
}

std::string make_putwords_message(const Player_Index player, List_of_Words_with_Points const& words_put_on_grid) noexcept
{
    json_type js = {
	"putwords",
	player.get(),
    };
    json_type array_of_words {};
    for (const auto& wp : words_put_on_grid)
    {
	json_type words_pair { wp.first, wp.second };
	array_of_words.push_back(words_pair);
    }
    js.push_back(array_of_words);

    return js.dump(JsonIndentation);
}

std::string make_givesup_message(Player_Index player) noexcept
{
    json_type js = {
	"givesup",
	player.get()
    };

    return js.dump(JsonIndentation);
}

std::string make_you_message(void) noexcept
{
    json_type js = {
	"you",
    };
    return js.dump(JsonIndentation);
}

std::string make_lolno_message(void) noexcept
{
    json_type js = {
	"lolno",
    };
    return js.dump(JsonIndentation);
}

std::string make_what_message(void) noexcept
{
    json_type js = {
	"what",
    };
    return js.dump(JsonIndentation);
}

Received_Messages parse_message_type(std::string const& message_from_client) noexcept
{
    try {
	json_type js = json_type::parse(message_from_client.c_str());
	const std::string message = js[0];
	if (message == "myattempt")
	{
	    return Received_Messages::My_Attempt;
	}
	else if (message == "skip")
	{
	    return Received_Messages::Skip;
	}
	else if (message == "myname")
	{
	    return Received_Messages::My_Name;
	}
	else
	{
	    return Received_Messages::Unknown;
	}
    }
    catch (...)
    {
	return Received_Messages::Unknown;
    }
}

//I don't really like resorting to macros, but the if(!cond) return false; idiom was so commonplace I had no other option.
#define CHECK_OR_RET_FALSE(condition)  if(!(condition)) return false;

/*! Check that the first two values of a tuple in a "myattempt" message describe a position in the Scrabble game grid
  \pre tuple contains at least two elements. Otherwise false will be returned.
  \param[in] tuple Represents a single tile (position and letter) in the JSON message.
 */
bool check_myattempt_valid_tile_position(json_type const& tuple) noexcept
{
    CHECK_OR_RET_FALSE(tuple.size() >= 2);
    //Scrabble tile positions start with 0, might as well check the coordinates are unsigned.
    const bool are_unsigned { tuple[0].is_number_unsigned() && tuple[1].is_number_unsigned() };
    CHECK_OR_RET_FALSE(are_unsigned);
    const unsigned int tile_x = tuple[0], tile_y = tuple[1];
    return (algo::is_in_range(0U, game::Rules::Grid_Width - 1, tile_x)
	    && algo::is_in_range(4U - 4, game::Rules::Grid_Height - 1, tile_y));
}

/*! Check that a tuple in a "myattempt" message describes a valid Scrabble tile (blank or letter)
  \pre tuple contains enough elements to store the tile value (tile value is on element 3, and also on element 4 if the tile is blank). Otheriwse false is returned.
  */
bool check_myattempt_valid_tile_value(json_type const& tuple)
{
    const auto size = tuple.size();
    CHECK_OR_RET_FALSE(algo::is_in_range(3LU, 4LU, size));   // We want tuples with 3 or 4 elements.

    CHECK_OR_RET_FALSE(tuple[2].is_string());

    const std::string tile_value = tuple[2] ;
    CHECK_OR_RET_FALSE(tile_value.size() == 1);
    const char tile_value_char { tile_value.front() };
    CHECK_OR_RET_FALSE(game::letter_is_tile(tile_value_char));

    if (size == 4)
    {
	CHECK_OR_RET_FALSE(tuple[3].is_string());
	const std::string letter_string = tuple[3] ;
	CHECK_OR_RET_FALSE(letter_string.size() == 1);
	const char letter { letter_string.front() };
	return game::letter_is_tile(letter) && letter != game::blank_tile && tile_value_char == game::blank_tile;
    }
    return tile_value_char != game::blank_tile;
}

/*! Check that a parameter to the "myattempt" message is valid */
bool check_myattempt_tuple_parameter(json_type& tuple) noexcept
{
    if (!tuple.is_array())
    {
	return false;
    }
    return check_myattempt_valid_tile_position(tuple) && check_myattempt_valid_tile_value(tuple);
}

/*! Check if a "myattempt" message received from a client is valid (the parameters are valid) */
bool check_myattempt_correct(json_type& js) noexcept
{
    CHECK_OR_RET_FALSE(js.size() > 0);
    CHECK_OR_RET_FALSE(js[0] == json_type("myattempt"));
    const bool all_valid_tuples = std::all_of(js.begin() + 1, js.end(), check_myattempt_tuple_parameter);

    return all_valid_tuples;
}

/*! Extract a tile value and position from a JSON array found in a "myattempt" message
  \pre The JSON array holds three or four elements and is suitable for extracting data. This will not be checked!
 */
Tile_with_Position my_attempt_message_json_tuple_to_tile_with_position(json_type const& tuple)
{
    const unsigned int tile_x = tuple[0], tile_y = tuple[1];
    const Ranged_uint<0, game::Rules::Grid_Width - 1> tile_x_ranged { tile_x };
    const Ranged_uint<0, game::Rules::Grid_Height - 1> tile_y_ranged { tile_y };
    std::string tile = tuple[2];
    char tuple_char { tile.front() };
    const bool is_blank { tuple_char == game::blank_tile };
    if (is_blank)
    {
	tile = tuple[3];
	tuple_char = tile.front();
    }
    const Tile_with_Position return_value {game::Grid_Pos{tile_x_ranged, tile_y_ranged}, game::Grid_Tile{tuple_char, is_blank}};
    return return_value;
}

Optional<Tiles_with_Positions> parse_myattempt_message(std::string const& message) noexcept
{
    json_type js = json_type::parse(message.c_str());
    Optional<Tiles_with_Positions> result {};
    if (check_myattempt_correct(js))
    {
	Tiles_with_Positions tiles_list { algo::transform(js.begin() + 1, js.end(),
							  my_attempt_message_json_tuple_to_tile_with_position) };
	result.emplace(tiles_list);
    }
    return result;
}

/*! Check if a "myname" message received from a client is valid (correct message name, and a unique string as a parameter) */
bool check_myname_valid(json_type& js) noexcept
{
    return js.size() == 2 && js[0] == "myname" && js[1].is_string();
}

Optional<std::string> parse_myname_message(std::string const& message) noexcept
{
    Optional<std::string> result {};
    try {
	json_type js = json_type::parse(message.c_str());
	if (check_myname_valid(js))
	{
	    const std::string name = js[1];
	    result.emplace(name);
	}
    }
    catch (...)
    {
	//Do nothing, the result will be empty anyway.
    }
    return result;
}

bool check_skip_message_correct(std::string const& message) noexcept {
    try {
	json_type js_parsed = json_type::parse(message.c_str());
	return js_parsed.size() == 1 && js_parsed[0] == json_type("skip");
    }
    catch (...)
    {
	return false;
    }
}

std::ostream& operator<<(std::ostream& os, Received_Messages mess) noexcept
{
    switch (mess)
    {
    case Received_Messages::My_Attempt:
    {
	os << "Received_Messages::My_Attempt";
	break;
    }
    case Received_Messages::Skip:
    {
	os << "Received_Messages::Skip";
	break;
    }
    case Received_Messages::My_Name:
    {
	os << "Received_Messages::My_Name";
	break;
    }
    default:
    {
	os << "Received_Messages::Unknown";
	break;
    }
    }
    return os;
}

}

}

}
