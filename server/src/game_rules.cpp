#include "game_rules.hpp"
#include "tile.hpp"

using namespace std;

namespace scrabble {
namespace game {

const std::map<char, unsigned int> French_Rules::Letters_Scoring {
    {'a', 1}, {'A', 1},
    {'b', 3}, {'B', 3},
    {'c', 3}, {'C', 3},
    {'d', 2}, {'D', 2},
    {'e', 1}, {'E', 1},
    {'f', 4}, {'F', 4},
    {'g', 2}, {'G', 2},
    {'h', 4}, {'H', 4},
    {'i', 1}, {'I', 1},
    {'j', 8}, {'J', 8},
    {'k', 10}, {'K', 10},
    {'l', 1}, {'L', 1},
    {'m', 2}, {'M', 2},
    {'n', 1}, {'N', 1},
    {'o', 1}, {'O', 1},
    {'p', 3}, {'P', 3},
    {'q', 8}, {'Q', 8},
    {'r', 1}, {'R', 1},
    {'s', 1}, {'S', 1},
    {'t', 1}, {'T', 1},
    {'u', 1}, {'U', 1},
    {'v', 4}, {'V', 4},
    {'w', 10}, {'W', 10},
    {'x', 10}, {'X', 10},
    {'y', 10}, {'Y', 10},
    {'z', 10}, {'Z', 10}
};

const std::map<char, unsigned int> French_Rules::Letter_Distribution
{
    {blank_tile, 2},
    {'a', 9}, {'A', 9},
    {'b', 2}, {'B', 2},
    {'c', 2}, {'C', 2},
    {'d', 3}, {'D', 3},
    {'e', 15}, {'E', 15},
    {'f', 2}, {'F', 2},
    {'g', 2}, {'G', 2},
    {'h', 2}, {'H', 2},
    {'i', 8}, {'I', 8},
    {'j', 1}, {'J', 1},
    {'k', 1}, {'K', 1},
    {'l', 5}, {'L', 5},
    {'m', 3}, {'M', 3},
    {'n', 6}, {'N', 6},
    {'o', 6}, {'O', 6},
    {'p', 2}, {'P', 2},
    {'q', 1}, {'Q', 1},
    {'r', 6}, {'R', 6},
    {'s', 6}, {'S', 6},
    {'t', 6}, {'T', 6},
    {'u', 6}, {'U', 6},
    {'v', 2}, {'V', 2},
    {'w', 1}, {'W', 1},
    {'x', 1}, {'X', 1},
    {'y', 1}, {'Y', 1},
    {'z', 1}, {'Z', 1}
};

//Aliases for the bonuses to define the grid. It would be even more hellish otherwise.
constexpr Grid_Tile_Bonus E = Grid_Tile_Bonus::None,
    B = Grid_Tile_Bonus::Game_Begin,
    L2 = Grid_Tile_Bonus::Double_Letter,
    L3 = Grid_Tile_Bonus::Triple_Letter,
    W2 = Grid_Tile_Bonus::Double_Word,
    W3 = Grid_Tile_Bonus::Triple_Word;

//Unreadable, I know. However a graphical display for the grid should easily prove its correctness.
const std::array<std::array<Grid_Tile_Bonus, French_Rules::Grid_Width>, French_Rules::Grid_Height> French_Rules::Grid_Bonuses =
{
{
    {{W3, E, E, L2, E, E, E, W3, E, E, E, L2, E, E, W3}},
    {{E, W2, E, E, E, L3, E, E, E, L3, E, E, E, W2, E}},
    {{E, E, W2, E, E, E, L2, E, L2, E, E, E, W2, E, E}},
    {{L2, E, E, W2, E, E, E, L2, E, E, E, W2, E, E, L2}},
    {{E, E, E, E, W2, E, E, E, E, E, W2, E, E, E, E}},
    {{E, L3, E, E, E, L3, E, E, E, L3, E, E, E, L3, E}},
    {{E, E, L2, E, E, E, L2, E, L2, E, E, E, L2, E, E}},
    {{W3, E, E, L2, E, E, E, B, E, E, E, L2, E, E, W3}},
    {{E, E, L2, E, E, E, L2, E, L2, E, E, E, L2, E, E}},
    {{E, L3, E, E, E, L3, E, E, E, L3, E, E, E, L3, E}},
    {{E, E, E, E, W2, E, E, E, E, E, W2, E, E, E, E}},
    {{L2, E, E, W2, E, E, E, L2, E, E, E, W2, E, E, L2}},
    {{E, E, W2, E, E, E, L2, E, L2, E, E, E, W2, E, E}},
    {{E, W2, E, E, E, L3, E, E, E, L3, E, E, E, W2, E}},
    {{W3, E, E, L2, E, E, E, W3, E, E, E, L2, E, E, W3}},
}
}
;

}
}
