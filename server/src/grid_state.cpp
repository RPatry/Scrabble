#include "grid_state.hpp"
#include "algo.hpp"
#include <locale>
#include <algorithm>

namespace scrabble {
namespace game {

char Grid_Tile::get_alphabet_char_or_throw(const char letter)
{
    const int lower_as_int { std::tolower(letter) };
    if (lower_as_int == EOF)
    {
	throw Not_in_alphabet{};
    }
    const char lower_letter { static_cast<char>(lower_as_int) };
    const std::string alphabet{"abcdefghijklmnopqrstuvwxyz"};
    if (std::find(alphabet.begin(), alphabet.end(), lower_letter) != alphabet.end())
    {
	return lower_letter;
    }
    else
    {
	throw Not_in_alphabet{};
    }
}

std::ostream& operator<<(std::ostream& os, Grid_Tile const& tile) noexcept
{
    os << "{'" << tile.letter << '\'';
    if (tile.is_blank)
    {
	os << "(blank)";
    }
    return os << '}';
}

std::ostream& operator<<(std::ostream& os, Grid_Pos const& pos) noexcept
{
    return os << "{" << pos.x << ", " << pos.y << "}";
}

bool check_no_duplicate_position(std::vector<Grid_Pos> const& positions) noexcept
{
    //Each possible position in the grid has an associated integer, which we can use to determine if there are any duplicates
    std::vector<int> real_positions{};
    for (const auto& pos : positions)
    {
	real_positions.push_back(pos.y * Rules::Grid_Height + pos.x);
    }
    return !algo::check_duplicates(std::move(real_positions));
}

bool check_all_grid_tiles_empty(Grid_State const& grid, std::vector<Grid_Pos> const& positions) noexcept
{
    return all_of(positions.begin(), positions.end(), [&grid](const Grid_Pos pos)
	{
	    return !grid.get_tile(pos).is_initialized();
	}
    );
}

Optional<Grid_State> Grid_State::add_tiles(std::vector<std::pair<Grid_Pos, Grid_Tile>> const& tiles_to_add) const noexcept
{
    Optional<Grid_State> return_value;
    const std::vector<Grid_Pos> positions{algo::transform(tiles_to_add.begin(), tiles_to_add.end(), [](const std::pair<Grid_Pos, Grid_Tile> pair) { return pair.first; } )};
    const bool no_duplicates { check_no_duplicate_position(positions) };
    const bool all_tiles_empty { check_all_grid_tiles_empty(*this, positions) };
    if (no_duplicates && all_tiles_empty)
    {
	auto new_tiles = this->tiles;
	for (const auto& tile : tiles_to_add)
	{
	    /*! Put the tile in the correct position in the list of tiles */
	    const auto& pos = tile.first;
	    const auto pos_in_vector = pos.y * Rules::Grid_Width + pos.x;
	    new_tiles[pos_in_vector].emplace(tile.second);
	}
	return_value.emplace(new_tiles);
    }
    return return_value;
}

}
}
