#ifndef GAME_RULES_HPP
#define GAME_RULES_HPP

#include <map>
#include <array>

namespace scrabble {
namespace game {

/*! Describes all the possible bonuses for a player move that a grid tile can bring */
enum class Grid_Tile_Bonus
{
    /*! No bonus on this tile */
    None,
    /*! The game must begin with a word over this tile */
    Game_Begin,
    /*! A letter put over this tile for the first time will be worth twice as much */
    Double_Letter,
    /*! A letter put over this tile for the first time will be worth three times as much */
    Triple_Letter,
    /*! A word put over this tile for the first time will be worth twice as much */
    Double_Word,
    /*! A word put over this tile for the first time will be worth three times as much */
    Triple_Word,
};

/*! The rules for the French version of Scrabble.
 */
struct French_Rules {
public:
    static const unsigned int Grid_Width = 15;
    static const unsigned int Grid_Height = 15;

    /*! Stores how much a letter is worth on its own, in the French version of Scrabble.
      Both lower-case and upper-case letters are accepted.
      A blank tile is worth zero points, but this is not stored here.
 */
    static const std::map<char, unsigned int> Letters_Scoring;

    /*! Stores how many times a letter appears in the whole deck of tiles in the game.
      Both lower-case and upper-case letters are accepted, as well as the blank tile.
      \sa blank_tile
    */
    static const std::map<char, unsigned int> Letter_Distribution;

    static const unsigned int Min_Number_Players = 2;
    static const unsigned int Max_Number_Players = 4;

    /*! Stores the bonus for each tile, row-major */
    static const std::array<std::array<Grid_Tile_Bonus, Grid_Width>, Grid_Height> Grid_Bonuses;
};

/*! The default rules are the French ones, of course */
using Rules = French_Rules;

}
}

#endif
