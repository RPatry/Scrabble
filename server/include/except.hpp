#ifndef EXCEPT_HPP
#define EXCEPT_HPP

namespace scrabble {

/*! The base Exception for the project. */
struct base_scrabble_exception {
public:
    virtual ~base_scrabble_exception() {  };
};

}

#endif // EXCEPT_HPP
