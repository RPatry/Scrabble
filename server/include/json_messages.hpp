#ifndef JSON_MESSAGES_HPP
#define JSON_MESSAGES_HPP

#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include "ranged/ranged.hpp"
#include "except.hpp"
#include "optional.hpp"

namespace scrabble {

namespace game {
struct Grid_Pos;
struct Grid_Tile;
enum class Grid_Tile_Bonus;
}

/*! Deal with the network part of the game: send and receive messages, handle TCP connections, etc. */
namespace network {

/*! Set of functions to decrypt received messages and build messages to send */
namespace messages {

using Player_Index = Ranged_uint<1, 4>;
using Tile_with_Position = std::pair<const game::Grid_Pos, const game::Grid_Tile>;
using Tiles_with_Positions = std::vector<Tile_with_Position>;

}
}
}

namespace std {
//Put in std for name lookup.
std::ostream& operator<<(std::ostream& os, scrabble::network::messages::Tile_with_Position const& twp) noexcept;
}

namespace scrabble {

namespace network {

namespace messages {

/*! Build a message to inform a player whether his name is valid or not (basically, whether he is the first to claim this name) */
std::string make_namevalid_message(bool name_valid) noexcept;

/*! Build a message to inform a player of the names of all the players in this game.
  \param names_players The names of the players in the correct order, that is, the first string will be the name for player 1, the second string for player 2, and so on.
 */
std::string make_allnames_message(std::vector<std::string> const& names_players) noexcept;

/*! Convert a Grid_Tile_Bonus to a textual representation appropriate to send over the network */
std::string gridbonus_to_string(game::Grid_Tile_Bonus bonus) noexcept;

/*! Build a message to inform a player of the contents of the bonus he can expect on each tile of the grid. */
std::string make_gridbonuses_message(std::vector<std::vector<game::Grid_Tile_Bonus>> const& grid_bonuses) noexcept;

/*! Build a message to inform a player of the tile that was randomly picked for him. This is at the beginning of the game to determine the player's ordering.
  \param tiles The tiles for the players. The first tile is for player 1, second tile for player 2, and so on. The client will know who is player 1,2,3... thanks to the order given in the "allnames" message.
 */
std::string make_tilespicked_message(std::vector<char> tiles) noexcept;

/*! Build a message to inform a client of the order in which players get to play.
  \param ordering A list of numbers in [1:number_players], each number represents the player's turn. Example: with [3, 1, 2], player 2 plays first, player 3 is next, and player 1 is last. The client will know who is player 1,2,3... thanks to the order given in the "allnames" message.
*/
std::string make_playersorder_message(std::vector<unsigned int> ordering) noexcept;

/*! Build a message to inform other players that a player just put down tiles on the grid. Client are supposed to remember this information, and if they later
  receive a putwords message, update their interior game model accordingly.
  \param tiles_with_positions A list of the form [(1, 3, 'a'), (1, 4, 'c'), (1, 5, '_', 't'), etc.], which lists tiles along with their positions(x, y)
*/
std::string make_attemptby_message(Player_Index player, bool move_valid, Tiles_with_Positions const& tiles_with_positions) noexcept;

using Word_with_Points = std::pair<std::string, unsigned int>;
using List_of_Words_with_Points = std::vector<Word_with_Points>;

/*! Inform a player who just played whether or not his move was valid. If it is valid, tell him his new deck of tiles, and which words he just played, along
  with their worth
*/
std::string make_yourresults_message(bool move_valid,
				     std::vector<std::string> const& new_letters = std::vector<std::string>{},
				     List_of_Words_with_Points const& words_put_on_grid = List_of_Words_with_Points{}) noexcept;

/*! Build a message to inform other players that what a player just played was successful, and to send them the words the player put down on the grid,
  along with how many points they were worth
*/
std::string make_putwords_message(Player_Index player, List_of_Words_with_Points const& words_put_on_grid) noexcept;

/*! Build a message to inform other players that a given player passed his turn.
  \param player The index of the player who passed his turn
*/
std::string make_givesup_message(Player_Index player) noexcept;

/*! Build a message to inform a player it is his turn to play */
std::string make_you_message(void) noexcept;

/*! Build a reply message to inform a player he was not supposed to talk to the server right now */
std::string make_lolno_message(void) noexcept;

/*! Build a message to inform a player that the message he sent was complete gibberish */
std::string make_what_message(void) noexcept;


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////


/*! Lists the possible messages we can receive from a client */
enum class Received_Messages {
	/*! The client tries to play for his turn */
        My_Attempt,
	/*! The client skips his turn */
	Skip,
	/*! The client informs us of his name (before the game begins) */
	My_Name,
	/*! What the client sent was not a valid message */
	Unknown
};

/*! Write a Received_Message to a stream. */
std::ostream& operator<<(std::ostream& os, Received_Messages mess) noexcept;

/*! Parse a message from a client and determine which message was received, for further later processing.
  Note that a message can still be erroneous even if we can determine its type.
 */
Received_Messages parse_message_type(std::string const& message_from_client) noexcept;

/*! Parse a "skip" message received from a client, and extract its parameter data.
  Be careful, this function only cares that the message is syntatically correct. Duplicate tiles, or semantic errors like that, are not checked.
  \return The list of tiles with their positions if the message is valid, an empty Optional otherwise.
 */
Optional<Tiles_with_Positions> parse_myattempt_message(std::string const& message) noexcept;

/*! Parse a "myname" message received from a client, and extract the parameter data (the name of the player).
  \return The name of the client if the message was valid, an empty Optional otherwise.
*/
Optional<std::string> parse_myname_message(std::string const& message) noexcept;

/*! Parse a "skip" message received from a client, and check that it was correctly formed (no parameters) */
bool check_skip_message_correct(std::string const& message) noexcept;

}

}

}

#endif // JSON_MESSAGES_HPP
