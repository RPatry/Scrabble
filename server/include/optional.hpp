#ifndef OPTIONAL_HPP
#define OPTIONAL_HPP

#include "boost/optional.hpp"

template<typename OptionalType>
using Optional = boost::optional<OptionalType>;


#endif // OPTIONAL_HPP
