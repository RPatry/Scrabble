#ifndef PAST_MOVE_HPP
#define PAST_MOVE_HPP

#include <string>
#include <vector>

namespace scrabble {
namespace game {

/*! Represents a word put by a player during a turn. It's not intended to be mutated
  as it represents "history" in a way.
 */
struct Word_Move {
public:
    /*! Build a Word_Move */
    explicit Word_Move(std::string const& word, const unsigned int score) :
	word{word}, score{score}
    {    }
    
    /*! The word put by the player on the grid */
    const std::string word{};

    /*! What the word was worth on the grid */
    const unsigned int score{};
};

/*! Represents a move made by a player during a turn */
struct Past_Move {
public:
    /*! Build a Past_Move */
    explicit Past_Move(std::vector<Word_Move> const& words_this_turn, const bool has_passed = false) :
	words_of_turn{words_this_turn}, has_passed{has_passed}
    {    }

    /*! All the words the player wrote on the grid using his tiles for this turn */
    const std::vector<Word_Move> words_of_turn{};

    /*! True if the player passed in this turn*/
    const bool has_passed { false };
};

}
}


#endif // PAST_MOVE_HPP
