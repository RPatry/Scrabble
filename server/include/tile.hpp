#ifndef TILE_HPP
#define TILE_HPP

#include <algorithm>
#include <string>
#include "except.hpp"

/*! Contains all the code for the scrabble backend
 */
namespace scrabble {

/*! Represents the core game logic
*/
namespace game {

/*! The letter representing a blank tile.
*/
const char blank_tile = '_';

/*! Check if a letter is a valid Scrabble tile (an alphabet letter, or a blank tile)
    */
static inline bool letter_is_tile(const char letter) noexcept
{
    using namespace std;
    static const string valid_letters {string{"abcdefghijklmnopqrstuvwxyz"} + blank_tile};
    return find(valid_letters.begin(), valid_letters.end(), letter)
	!= valid_letters.end();
}

/*! Thrown if trying to construct a Tile with an invalid letter
    \sa Tile::Tile()
    */
struct Invalid_Tile : base_scrabble_exception
{
};

/*! Represent a Scrabble tile (either a letter of the alphabet, or the blank tile),
  before it is put on the grid.
    */
struct Tile {
public:
    /*! Try to construct a Tile with the given letter.
	Will throw with Invalid_Tile if letter_is_tile returns false for the letter.
	\sa letter_is_tile(const char) noexcept, Invalid_Tile
	*/
    explicit Tile(const char letter) : letter{get_letter_or_throw(letter)}
    {    }

    /*! The letter represented by this tile
	*/
    const char letter;

private:
    char get_letter_or_throw(const char letter) const
    {
	if (letter_is_tile(letter))
	{
	    return letter;
	}
	else
	{
	    throw Invalid_Tile{};
	}
    }
};
}
}

#endif // TILE_HPP
