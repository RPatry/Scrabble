#ifndef ALGO_HPP
#define ALGO_HPP

#include <vector>
#include <algorithm>
#include <type_traits>

/*! Custom algorithms usually built over std algorithms. */
namespace algo {

/*! Look through a range of values, check if at least one value is duplicated.
  Val must be a Comparable type.
  \return true if at least one value is present at least twice in the range.
*/
template<typename Val, typename Alloc>
bool check_duplicates(std::vector<Val, Alloc> values) noexcept
{
    std::sort(values.begin(), values.end());
    return std::adjacent_find(values.begin(), values.end()) != values.end();
}

/*! Simplifies notation for std::iterator_traits::value_type */
template<typename Iter>
using Iter_Value_Type = typename std::iterator_traits<Iter>::value_type;

/*! Simplifies notation for std::result_of */
template<typename FunctionPrototype>
using Result_Of = typename std::result_of<FunctionPrototype>::type;

/*! Use the standard transform algorithm over a range of values, and return a vector of the results.
  It simplifies notation a bit.
  Example: \code{.cpp}
  std::vector<int> values { 5, 9, 4, 11, 3, 6 };
  std::vector<bool> value_is_over_seven { algo::transform(values.begin(), values.end(), [](const int val) {
      return val > 7;
  }) };
  \endcode
  \tparam IterIn An iterator type.
  \tparam UnaryOperation A functor which takes a single argument of the type pointed to by a IterIn.
  \return A vector of the results. The type of vector is deduced from the return result of UnaryOperation.
*/
template<typename UnaryOperation, typename IterIn>
std::vector<Result_Of<UnaryOperation(Iter_Value_Type<IterIn>)>>
  transform(IterIn first, IterIn last, UnaryOperation op)
{
    std::vector<Result_Of<UnaryOperation(Iter_Value_Type<IterIn>)>> result{};
    std::transform(first, last, std::back_inserter(result), op);
    return result;
}

/*! Check if a value is in the range [low, high]
  \tparam LessOrEqualComparisonFunctor A functor taking (a, b) as parameters checking that a <= b
 */
template<typename RangeType, typename LessOrEqualComparisonFunctor = std::less_equal<RangeType>>
    bool is_in_range(RangeType low, RangeType high, RangeType value)
{
    const LessOrEqualComparisonFunctor is_less_or_equal{};
    return is_less_or_equal(low, value) && is_less_or_equal(value, high);
}

}

#endif // ALGO_HPP
