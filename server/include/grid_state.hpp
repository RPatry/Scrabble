#ifndef GRID_STATE_HPP
#define GRID_STATE_HPP

#include "tile.hpp"
#include "game_rules.hpp"
#include "ranged/ranged.hpp"
#include "optional.hpp"
#include "except.hpp"
#include <algorithm>
#include <iostream>

namespace scrabble {
namespace game {

/*! Thrown if a Grid_Tile is built with a letter not in the alphabet.
  \sa Grid_Tile::Grid_Tile()
*/
struct Not_in_alphabet : base_scrabble_exception
{
};

/*! Information about a tile which was put down on the grid */
struct Grid_Tile {
public:
    /*! Build a Grid_Tile object
      \param [in] _letter Indicates the letter of the tile, or the letter the player means to play if the tile is blank.
      \param [in] _is_blank Indicates whether the tile is blank.
      \exception Not_in_alphabet _letter does not belong to the latin alphabet
    */
    explicit Grid_Tile(const char _letter, const bool _is_blank = false) :
	letter{get_alphabet_char_or_throw(_letter)}, is_blank{_is_blank}
    {
    }
    
    /*! The letter of the tile, must belong to the alphabet */
    const char letter;

    /*! True if the tile is blank. If true, letter indicates the character that a player actually wants to use in place of the blank tile */
    const bool is_blank;

    bool operator==(Grid_Tile const& tile2) const noexcept
    {
	return this->letter == tile2.letter
	    && this->is_blank == tile2.is_blank;
    }

private:
    static char get_alphabet_char_or_throw(const char letter);
};

/*! Output a Grid_Tile on a stream. */
std::ostream& operator<<(std::ostream& os, Grid_Tile const&) noexcept;

/*! A tile position, guaranteed to be inside the grid thanks to Ranged_uint */
struct Grid_Pos {
    /*! Build a Grid_Pos */
    explicit Grid_Pos(const Ranged_uint<0, Rules::Grid_Width - 1> _x, const Ranged_uint<0, Rules::Grid_Height - 1> _y) noexcept :
	x{_x}, y{_y}
    {    }
    /*! The column of a tile */
    const Ranged_uint<0, Rules::Grid_Width - 1> x{};
    /*! The line of a tile */
    const Ranged_uint<0, Rules::Grid_Height - 1> y{};

    /*! Compare two Grid_Pos */
    bool operator==(const Grid_Pos pos) const noexcept
    {
	return this->x == pos.x && this->y == pos.y;
    }
};

/*! Thrown if you try to build a Grid_State with not enough/too many tiles.
  \sa Grid_State::Grid_State(std::vector<Optional<Grid_Tile>> const&)
*/
struct Invalid_grid_size : base_scrabble_exception
{
};

/*! Stores information about which tiles are occupied with which letter, at a particular point in time. */
struct Grid_State {
public:
    /*! Build a new Grid_State with the specified tiles.
      \exception Invalid_grid_size grid.size() == (Grid_Width * Grid_Height) is not respected.
     */
    explicit Grid_State(std::vector<Optional<Grid_Tile>> const& grid) :
	tiles{grid}
    {
	if (grid.size() != Number_Tiles)
	{
	    throw Invalid_grid_size{};
	}
    }

    explicit Grid_State(void) :
	tiles{Number_Tiles}
    {    }

    /*! Add the tiles of a player on the Grid. Return a modified copy of the grid. The optional Grid_State will be empty if there was an error. */
    Optional<Grid_State> add_tiles(std::vector<std::pair<Grid_Pos, Grid_Tile>> const& tiles) const noexcept;

    /*! Read the value of a tile.
     \return The possibly empty tile value.*/
    Optional<Grid_Tile> get_tile(const Grid_Pos pos) const noexcept
    {
	return tiles[pos.y * Rules::Grid_Width + pos.x];
    }

    static const size_t Number_Tiles {Rules::Grid_Width * Rules::Grid_Height};

private:
    /*! The tiles stored in the grid, stored row by row */
    const std::vector<Optional<Grid_Tile>> tiles{};
};

/*! Output a Grid_Pos on a stream. */
std::ostream& operator<<(std::ostream& os, Grid_Pos const&) noexcept;

/*! Check if no position is present twice in a list of positions */
bool check_no_duplicate_position(std::vector<Grid_Pos> const& positions) noexcept;

/*! Check if all positions point to empty tiles in the grid */
bool check_all_grid_tiles_empty(Grid_State const&, std::vector<Grid_Pos> const& positions) noexcept;

}
}

#endif
