#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include <vector>
#include "tile.hpp"

namespace scrabble {
namespace game {

/*! Models data about a player of scrabble.
    */
struct Player {
    /*! The current set of letters a player owns */
    const std::vector<Tile> deck{};   //Perhaps make it its own separate class?
    /*! The name of the player */
    const std::string name{};
};

}
}

#endif // PLAYER_HPP
