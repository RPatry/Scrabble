#ifndef DICT_UNORDERED_SET_HPP
#define DICT_UNORDERED_SET_HPP

#include <vector>
#include <string>
#include <unordered_set>
#include "boost/algorithm/string.hpp"

//Satisfies the standard Hash concept: http://en.cppreference.com/w/cpp/concept/Hash
class icase_hash
{
public:

    std::size_t operator()(std::string word) const noexcept
    {
        boost::to_lower(word);
        const std::hash<std::string> hash_object{};
        return hash_object(word);
    }
};

class icase_equal
{
public:
    bool operator()(std::string const& a, std::string const& b) const noexcept
    {
        return boost::iequals(a, b);
    }
};

class dict_unordered_set
{
public:
    dict_unordered_set(std::vector<std::string> const& words) noexcept;
    bool word_exists(std::string word) const noexcept;

private:
    using Unordered_Set_Type = std::unordered_set<std::string, icase_hash, icase_equal>;
    const Unordered_Set_Type words{};

    Unordered_Set_Type make_words_set(std::vector<std::string> const& words) const noexcept;
    
};


#endif // !1