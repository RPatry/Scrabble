#ifndef DICT_VECTOR_SORTED_HPP
#define DICT_VECTOR_SORTED_HPP

#include <string>
#include <vector>

class dict_vector_sorted
{
public:
    dict_vector_sorted(std::vector<std::string> const& words) noexcept;
    bool word_exists(std::string word) const noexcept;

private:
    const std::vector<std::string> words{};

    std::vector<std::string> sort_words(std::vector<std::string> words) const noexcept;
    
};


#endif // !1