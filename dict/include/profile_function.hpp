#ifndef PROFILE_FUNCTION_HPP
#define PROFILE_FUNCTION_HPP

#include <chrono>

template<typename Function, typename... FunctionArgs>
std::chrono::nanoseconds profile_function_nano(Function function, FunctionArgs... args) noexcept
{
    using namespace std::chrono;
    
    //Profiling
    const auto begin = steady_clock::now();
    function(args...);
    const auto end = steady_clock::now();

    //Computing the time it took
    const auto time_spent = end - begin;

    return duration_cast<std::chrono::nanoseconds>(time_spent);
}

#endif // PROFILE_FUNCTION_HPP