#ifndef DICT_VECTOR_HPP
#define DICT_VECTOR_HPP

#include <vector>
#include <string>

class dict_vector
{
public:
    dict_vector(std::vector<std::string> const& words) noexcept;
    bool word_exists(std::string word) const noexcept;

private:
    const std::vector<std::string> words{};
    
};

#endif // !1