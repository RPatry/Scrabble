#ifndef DICT_MAP_FIRST_LETTER_HPP
#define DICT_MAP_FIRST_LETTER_HPP

#include <vector>
#include <map>
#include <string>

class dict_map_first_letter
{
public:
    dict_map_first_letter(std::vector<std::string> const& words) noexcept;
    bool word_exists(std::string word) const noexcept;

private:
    const std::map<char, std::vector<std::string>> words_per_first_letter{};

    std::map<char, std::vector<std::string>> make_words_list(std::vector<std::string> const& words) const noexcept;

};


#endif // !1