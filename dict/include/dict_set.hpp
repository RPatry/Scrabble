#ifndef DICT_SET_HPP
#define DICT_SET_HPP

#include <vector>
#include <string>
#include <set>
#include "boost/algorithm/string.hpp"

//Should satisfy the Compare concept : http://en.cppreference.com/w/cpp/concept/Compare
class icase_compare
{
public:
    bool operator()(std::string const& a, std::string const& b) const noexcept
    {
        return boost::ilexicographical_compare(a, b);
    }
};

class dict_set
{
public:
    dict_set(std::vector<std::string> const& words) noexcept;
    bool word_exists(std::string word) const noexcept;

private:
    using Set_Type = std::set<std::string, icase_compare>; 
    const Set_Type words{};
    
    Set_Type make_words_set(std::vector<std::string> const& words) const noexcept;
    
};


#endif // !1
