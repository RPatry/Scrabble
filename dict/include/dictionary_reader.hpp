#ifndef DICTIONARY_READER_HPP
#define DICTIONARY_READER_HPP

#include <fstream>
#include <string>
#include <vector>

using list_of_words = std::vector<std::string>;

list_of_words read_words_from_dictionary(std::ifstream& inputStreamDictionary, char wordSeparator = ' ',
                      char lineSeparator = '\n', bool skipFirstLine = true);


#endif // DICTIONARY_READER_HPP