##### Functions

#map, called like this: $(map myfunc, mylist)
map = $(foreach t, $(2), $(call $(1),$(t)))

#map_1, called like this: $(map myfunc, mylist, param) where myfunc takes two parameters: the first is param, the second an item from mylist.
map_1 = $(foreach t, $(2), $(call $(1),$(3),$(t)))

#Find all the files whose name match a pattern, recursively in a directory. The advantage over using "find" directly is portability (on Windows mainly).
rec_wildcard = $(wildcard $(1)$(2)) $(foreach d,$(wildcard $(1)*), $(call rec_wildcard,$(d)/,$(2)))

#Find all the subdirectories names of a directory
subdirs = $(sort $(dir $(call rec_wildcard,$(1)/,*)))

##### The Makefile itself

##There are two targets: debug and release, each with specific directories and compiler flags. 
##But the building procedure is nearly identical for both.

##To build the executable, you'll need boost. Any recent version should suffice.
##Don't forget to add the path to boost in the compiler include path.

SRCDIR := src
INCDIR := include
CXX := g++
##Add path to boost here!!!
INC := -I./$(INCDIR) -I../../boost_1_61_0/
LDLIBS := 
CXXFLAGS := -std=c++11 -Wall -Wextra -pedantic -save-temps=obj $(INC)
RFLAGS := -O2
DFLAGS := -g
DEBUG_DIR = debugt
RELEASE_DIR = releaset
SRCSUBDIRS := $(call subdirs,$(SRCDIR))
DEPSDIR := .deps
DEPSDIRS := $(DEPSDIR) $(addprefix  .deps/$(DEBUG_DIR)/, $(SRCSUBDIRS))   $(addprefix .deps/$(RELEASE_DIR)/, $(SRCSUBDIRS))
OBJDIRS := obj $(addprefix  obj/$(DEBUG_DIR)/, $(SRCSUBDIRS))   $(addprefix obj/$(RELEASE_DIR)/, $(SRCSUBDIRS))
BINDIRS := bin bin/$(DEBUG_DIR) bin/$(RELEASE_DIR)

EXEC := TestDictServer

BAD_MAIN_FILES := $(SRCDIR)/main.cpp $(SRCDIR)/profile-main.cpp
SRCALL := $(call rec_wildcard,$(SRCDIR)/,*.cpp)
SRC := $(filter-out $(BAD_MAIN_FILES), $(SRCALL))
ASM := $(call rec_wildcard,$(SRCDIR)/,*.s)
DOBJS := $(SRC:$(SRCDIR)/%.cpp=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o)
ROBJS := $(SRC:$(SRCDIR)/%.cpp=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o)
OBJS := $(DOBJS) $(ROBJS)
DDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/$(DEBUG_DIR)/$(SRCDIR)/%.cpp.d)
RDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/$(RELEASE_DIR)/$(SRCDIR)/%.cpp.d)
DEPS := $(DDEPS) $(RDEPS)
OUTASM := $(SRC:$(SRCDIR)/%.cpp=obj/$(RELEASE_DIR)/%.s) $(SRC:%.cpp=obj/$(DEBUG_DIR)/%.s)
II := $(SRC:%.cpp=obj/$(RELEASE_DIR)/%.ii) $(SRC:%.cpp=obj/$(DEBUG_DIR)/%.ii)

.PHONY: all debug release
all: setup debug release

-include $(DEPS)

debug: bin/$(DEBUG_DIR)/$(EXEC)
release: bin/$(RELEASE_DIR)/$(EXEC)

bin/$(DEBUG_DIR)/$(EXEC): $(DOBJS)
	$(CXX) -o $@ $^ $(LDLIBS)  ../../boost_1_61_0/boost/bin.v2/libs/test/build/gcc-mingw-5.2.0/debug/libboost_unit_test_framework-mgw52-d-1_61.dll

bin/$(RELEASE_DIR)/$(EXEC): $(ROBJS)
	$(CXX) -o $@ $^ -s $(LDLIBS) ../../boost_1_61_0/boost/bin.v2/libs/test/build/gcc-mingw-5.2.0/release/libboost_unit_test_framework-mgw52-1_61.dll

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/$(DEBUG_DIR)/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(RFLAGS) -MM -MF .deps/$(RELEASE_DIR)/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

.PHONY: setup
setup: 
	mkdir -p $(DEPSDIRS) $(BINDIRS) $(OBJDIRS)

.PHONY: clean
clean:
	rm -f $(OBJS) bin/$(DEBUG_DIR)/$(EXEC) bin/$(RELEASE_DIR)/$(EXEC) $(OUTASM) $(II) $(DEPS)
