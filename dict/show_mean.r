#path_file_csv = "profiling-Windows-novector-noinline.csv"
path_file_csv = "profile-results-Linux-novector-noinline-nofinalsemicolon.csv"
csv_profile = na.omit(read.csv(path_file_csv, sep=";"))
csv_profile$firstLetter = substr(csv_profile$Words, start=1, stop=1)
lengths_words = sapply(csv_profile$Words, function(word) {
		       nchar(as.character(word))
})
csv_profile[,"lengthWord"] = lengths_words
mean_total = sapply(colnames(csv_profile)[2:5], function(col) {
    mean(csv_profile[,col])
})

#Average by word length
mean_wd_len = sapply(sort(unique(lengths_words)), function(len) {
       sapply(colnames(csv_profile)[2:5], function(col) {
	      mean(csv_profile[csv_profile$lengthWord == len, col])
	})
})

#Average by first letter
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
v_alphabet = strsplit(alphabet, "")[[1]]
mean_first_letter = sapply(v_alphabet, function(letter) {
       sapply(colnames(csv_profile)[2:5], function(col) {
	      mean(csv_profile[csv_profile$firstLetter == letter, col])
	})
})

#I think now you can sapply over the lengths_words and csv_profile$firstLetter and apply mean over csv_profile[,"lengthWord" == this_length]
#Perhaps. Something like that.
