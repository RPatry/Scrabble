#include "dictionary_reader.hpp"
#include "boost/algorithm/string/split.hpp"


list_of_words read_words_from_dictionary(std::ifstream& inputStreamDictionary, const char wordSeparator,
                      const char lineSeparator, const bool skipFirstLine)
{
    using namespace std;
    
    ifstream& dictionary{inputStreamDictionary};
    string line{};
    list_of_words words{};
    
    if (skipFirstLine)
    {
        getline(dictionary, line, lineSeparator);   //Read a line and forget it.
    }
    while (getline(dictionary, line, lineSeparator))
    {
        vector<string> words_of_line{};
        boost::split(words_of_line, line, [=](const char c) { return c == wordSeparator; },
                boost::token_compress_on);
        words.insert(words.end(), words_of_line.begin(), words_of_line.end());
        /*
        for (auto& word : words_of_line)
        {
            structure.append_word(move(word));
        }
        */
    }
    return words;
}
