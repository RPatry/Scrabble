#include "dict_set.hpp"

using namespace std;

dict_set::dict_set(vector<string> const& words) noexcept : words{make_words_set(words)}
{    }

bool dict_set::word_exists(const std::string word) const noexcept
{
    return this->words.find(word) != this->words.end();
    /*
    return this->words.end() != find_if(this->words.begin(), this->words.end(), [=](string const& w) {
        return boost::iequals(word, w);
    });*/
}

dict_set::Set_Type dict_set::make_words_set(vector<string> const& words) const noexcept
{
    return dict_set::Set_Type{words.begin(), words.end()};
}