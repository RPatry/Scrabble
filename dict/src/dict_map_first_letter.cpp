#include "dict_map_first_letter.hpp"
#include "boost/algorithm/string.hpp"
#include <locale>
#include <algorithm>

using namespace std;

dict_map_first_letter::dict_map_first_letter(vector<string> const& words) noexcept : 
        words_per_first_letter{make_words_list(words)}
{  }

bool dict_map_first_letter::word_exists(const string word) const noexcept
{
    bool result { true };
    try
    {
        const vector<string> & words_with_first_letter { this->words_per_first_letter.at(tolower(word.at(0))) };
        result = words_with_first_letter.end() !=
            find_if(words_with_first_letter.begin(), words_with_first_letter.end(), [=] (string const& w) {
                return boost::iequals(w, word);
            });
    }
    catch (out_of_range&)    //We can pass here if word is empty, or if the first letter is not found in the map.
    {
        result = false;
    }
    return result;
}

void add_word_to_dictionary(map<char, vector<string>> &map_words, string const& word) noexcept
{
    if (word.size() > 0)
    {
        const char first_letter = tolower(word[0]);
        auto& list_of_words_with_first_letter = map_words[first_letter];
        list_of_words_with_first_letter.push_back(word);
    }
}

map<char, vector<string>> dict_map_first_letter::make_words_list(vector<string> const& words) const noexcept
{
    map<char, vector<string>> result{};
    for (auto& word : words)
    {
        add_word_to_dictionary(result, word);
    }
    return result;
}