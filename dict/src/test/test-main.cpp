#include "boost/test/included/unit_test.hpp"
#include <boost/mpl/vector.hpp>
#include "dict_map_first_letter.hpp"
#include "dict_set.hpp"
#include "dict_unordered_set.hpp"
#include "dict_vector.hpp"
#include "dict_vector_sorted.hpp"
#include <vector>
#include <string>

using Dictionary_Types = boost::mpl::vector<dict_map_first_letter, dict_set, dict_unordered_set, dict_vector, dict_vector_sorted>;

BOOST_AUTO_TEST_CASE(assert_true_is_true_thank_you_captain_obvious)
{
    BOOST_CHECK_EQUAL(true, true);
}

BOOST_AUTO_TEST_CASE(assert_set_key_compare_satisfies_stdcompare_concept)
{
    using namespace std;
    
    //Take a look at this : http://en.cppreference.com/w/cpp/concept/Compare

    //Note: this comparison object ignores case!
    
    icase_compare comp{};
    BOOST_CHECK(comp("abc", "bcd"));
    BOOST_CHECK(comp("ABC", "abcd"));
    BOOST_CHECK(!comp("bcd", "aBc"));
    BOOST_CHECK(!comp("abcd", "abc"));
    BOOST_CHECK(!comp("abc", "abc"));
    BOOST_CHECK(comp("bcd", "efg"));
    BOOST_CHECK(comp("abc", "efg"));

    //Property one (strict ordering) is now verified

    auto equiv = [&](string const& a, string const& b) {
        return !comp(a, b) && !comp(b, a);
    };
    BOOST_CHECK(equiv("abc", "abc"));
    BOOST_CHECK(equiv("abc", "ABC"));
    BOOST_CHECK(equiv("ABC", "abc"));

    //Property two (equivalence) verified, apart from transitivity
    
}

BOOST_AUTO_TEST_CASE(assert_icase_hash_satisfies_stdhash_concept)
{
    using namespace std;
    icase_hash hash{};
    //Execute a few times, check all hashes are same
    const string ref{"A string"};
    auto hash1 = hash(ref);
    auto hash2 = hash(ref);
    auto hash3 = hash(ref);
    BOOST_CHECK(hash1 == hash2 && hash2 == hash3);

    //We'll also check that the same hashes are given for similar strings, ignoring case.
    const string ref2{"a sTRiNG"}, ref3{"a string"}, ref4{"A STRING"}, ref5{"A StrINg"};
    vector<decltype(hash1)> hashes_icase{};
    const vector<decltype(hash1)> reference_hashes_icase(4, hash1);   //The results that ought to be found.
    for (const auto& str : {ref2, ref3, ref4, ref5})
    {
        hashes_icase.push_back(hash(str));
    }
    BOOST_CHECK_EQUAL_COLLECTIONS(hashes_icase.begin(), hashes_icase.end(), reference_hashes_icase.begin(), reference_hashes_icase.end());
}

BOOST_AUTO_TEST_CASE(check_icase_equals_compares_correctly)
{
    using namespace std;
    const icase_equal equal{};
    BOOST_CHECK(equal("a str", "A STR"));
    BOOST_CHECK(equal("A STR", "a str"));
    BOOST_CHECK(equal("a str", "A sTR"));
    BOOST_CHECK(!equal("a str", "another str"));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_words_can_be_found, Dictionary_Type, Dictionary_Types)
{
    using namespace std;
    const vector<string> adhoc_dictionary { "frog", "game", "extraordinary", "great", "eat", "viable", "out", "move", "hedge",
                                            "fund", "finance", "wall", "build", "hello", "world", "visibility", "viability",
                                            "war", "wars", "twist", "worm", "appreciate", "design", "dictionary" };
    const Dictionary_Type dictionary {adhoc_dictionary};
    for (const auto& word : adhoc_dictionary)
    {
        BOOST_CHECK_MESSAGE (dictionary.word_exists(word), "A word could not be found : " << word);
    }
    BOOST_CHECK_MESSAGE (dictionary.word_exists("FROG"), "FROG could not be found in the dictionary, word comparison is case-sensitive!");
    BOOST_CHECK_MESSAGE (dictionary.word_exists("EXtRaORdinARy"), "EXtRaORdinARy could not be found in the dictionary, word comparison is case-sensitive!");

    BOOST_CHECK_MESSAGE (!dictionary.word_exists("whale"), "Found a word in the dictionary which was not added in it!");
    BOOST_CHECK_MESSAGE (!dictionary.word_exists("eating"), "Found a word in the dictionary which was not added in it!");
}

boost::unit_test::test_suite* init_unit_test_suite( int , char* [] )
{
    return nullptr;
}