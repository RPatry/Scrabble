#include "dict_vector.hpp"
#include "boost/algorithm/string.hpp"
#include <algorithm>

using namespace std;

dict_vector::dict_vector(vector<string> const& words) noexcept : words{words}
{  }

bool dict_vector::word_exists(const std::string word) const noexcept
{
    return words.end() !=
        find_if(words.begin(), words.end(), [=](string const& w) {
            return boost::iequals(w, word);
        });
}