#include "dict_vector_sorted.hpp"
#include "boost/algorithm/string.hpp"
#include <algorithm>

using namespace std;

dict_vector_sorted::dict_vector_sorted(vector<string> const& _words) noexcept : words{sort_words(_words)}
{    }

bool dict_vector_sorted::word_exists(const string word) const noexcept
{
    return binary_search(this->words.begin(), this->words.end(), word, [](string const& s1, string const& s2) {
        return boost::ilexicographical_compare(s1, s2);
    });
}

vector<string> dict_vector_sorted::sort_words(vector<string> words_to_sort) const noexcept
{
    sort(words_to_sort.begin(), words_to_sort.end());
    return words_to_sort;
}