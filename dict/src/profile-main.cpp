#include "dictionary_reader.hpp"
#include "profile_function.hpp"
#include "dict_vector.hpp"
#include "dict_map_first_letter.hpp"
#include "dict_set.hpp"
#include "dict_unordered_set.hpp"
#include "dict_vector_sorted.hpp"
#include <fstream>
#include <algorithm>
#include <iostream>

using namespace std;

using ProfilingResults = vector<std::chrono::nanoseconds>;

template<typename DictionaryStructure>
ProfilingResults profile_structure(vector<string> const& words) noexcept
{
    ProfilingResults results{};
    const DictionaryStructure structure{words};
    results.reserve(words.size());
    for (const auto& word : words)
    {
        results.push_back(profile_function_nano([&](){ structure.word_exists(word); }));
    }

    return results;
}

void write_csv_line(ostream& os) noexcept
{
    os << '\n';
}

template<typename FirstArg, typename ...Args>
void write_csv_line(ostream& os, const FirstArg arg, Args... args) noexcept
{
    os << arg << ";";
    write_csv_line(os, args...);
}

/*
template<typename FirstArg>
void write_csv_line(ostream& os, const FirstArg arg)
{
    os << arg << '\n';
}*/

void write_csv(const string path_csv, vector<string> const& words, /*ProfilingResults const& results_vector,*/ ProfilingResults const& results_map_first_letter,
            ProfilingResults const& results_set, ProfilingResults const& results_unordered_set, ProfilingResults const& results_sorted_vector)
{
    ofstream csv_file{path_csv};
    write_csv_line(csv_file, "Words", /*"Vector",*/ "Map of first letter", "Set", "Unordered set", "Sorted vector");
    const auto sizes = {/*results_vector.size(),*/ results_map_first_letter.size(), results_set.size(),
                                        results_unordered_set.size(), results_sorted_vector.size(), words.size()};
    const auto nb_iter = *min_element(sizes.begin(), sizes.end());
    for (size_t i = 0; i < nb_iter; ++i)
    {
        write_csv_line(csv_file, words[i], /*results_vector[i].count(),*/ results_map_first_letter[i].count(), results_set[i].count(),
                        results_unordered_set[i].count(), results_sorted_vector[i].count());
    }
}

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        cerr << "Expected two arguments: path to the dictionary as a text file, and path to the output csv file" << endl;
        return -1;
    }
    ifstream dictionary_text{argv[1]};
    cout << "Starting to read the words!" << endl;
    const auto all_words = read_words_from_dictionary(dictionary_text, ' ', '\n', true);
    cout << "Read all the words! There are " << all_words.size() << " words." << endl;
    /*
    const auto results_vector = profile_structure<dict_vector>(all_words);
    cout << "Done profiling vector!" << endl;
    */
    const auto results_map_first_letter = profile_structure<dict_map_first_letter>(all_words);
    cout << "Done profiling map with first letter!" << endl;
    const auto results_set = profile_structure<dict_set>(all_words);
    cout << "Done profiling set!" << endl;
    const auto results_unordered_set = profile_structure<dict_unordered_set>(all_words);
    cout << "Done profiling unordered set!" << endl;
    const auto results_sorted_vector = profile_structure<dict_vector_sorted>(all_words);
    cout << "Done profiling sorted vector!" << endl;

    write_csv(argv[2], all_words,/* results_vector,*/ results_map_first_letter, results_set, results_unordered_set, results_sorted_vector);

    cout << "Done writing csv!\nDone!" << endl;

    return 0;
}
