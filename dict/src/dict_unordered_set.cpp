#include "dict_unordered_set.hpp"
#include <algorithm>

using namespace std;

dict_unordered_set::dict_unordered_set(vector<string> const& words) noexcept :
    words{make_words_set(words)}
{    }

bool dict_unordered_set::word_exists(const string word) const noexcept
{
    return this->words.find(word) != this->words.end();
    /*
    return this->words.end() !=
        find_if(this->words.begin(), this->words.end(), [=](string const& w ) {
            return boost::iequals(w, word);            
        });
        */
}

dict_unordered_set::Unordered_Set_Type dict_unordered_set::make_words_set(vector<string> const& words) const noexcept
{
    const dict_unordered_set::Unordered_Set_Type result{words.begin(), words.end()};

    return result;
}