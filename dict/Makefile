##### Functions

#map, called like this: $(map myfunc, mylist)
map = $(foreach t, $(2), $(call $(1),$(t)))

#map_1, called like this: $(map myfunc, mylist, param) where myfunc takes two parameters: the first is param, the second an item from mylist.
map_1 = $(foreach t, $(2), $(call $(1),$(3),$(t)))

##### The Makefile itself

SRCDIR := src
INCDIR := include
#g++ has to be >= 4.9, so as to avoid warnings when initialising member const references with another const reference in a constructor.
CXX := g++
INC := -I./$(INCDIR)
LDLIBS := 
CXXFLAGS := -std=c++11 -Wall -Wextra -Werror -pedantic -save-temps=obj $(INC)
RFLAGS := -O2
DFLAGS := -g
SRCSUBDIRS := $(SRCDIR) $(shell find $(SRCDIR) -type d)
DEPSDIR := .deps
DEPSDIRS := $(DEPSDIR) $(call map_1, addprefix, $(SRCSUBDIRS), .deps/debug/) $(call map_1, addprefix, $(SRCSUBDIRS), .deps/release/)
OBJDIRS := obj $(call map_1, addprefix, $(SRCSUBDIRS), obj/debug/) $(call map_1, addprefix, $(SRCSUBDIRS), obj/release/)
BINDIRS := bin bin/debug bin/release

EXEC := DictServer

SRC := $(shell find $(SRCDIR) -type f -name "*.cpp")
ASM := $(shell find $(SRCDIR) -type f -name "*.s")
DOBJS := $(SRC:$(SRCDIR)/%.cpp=obj/debug/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/debug/$(SRCDIR)/%.o)
ROBJS := $(SRC:$(SRCDIR)/%.cpp=obj/release/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/release/$(SRCDIR)/%.o)
OBJS := $(DOBJS) $(ROBJS)
DDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/debug/$(SRCDIR)/%.cpp.d)
RDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/release/$(SRCDIR)/%.cpp.d)
DEPS := $(DDEPS) $(RDEPS)
OUTASM := $(SRC:$(SRCDIR)/%.cpp=obj/release/%.s) $(SRC:%.cpp=obj/debug/%.s)
II := $(SRC:%.cpp=obj/release/%.ii) $(SRC:%.cpp=obj/debug/%.ii)

.PHONY: all debug release
all: setup debug release

-include $(DEPS)

debug: bin/debug/$(EXEC)
release: bin/release/$(EXEC)

bin/debug/$(EXEC): $(DOBJS)
	$(CXX) -o $@ $^ $(LDLIBS)

bin/release/$(EXEC): $(ROBJS)
	$(CXX) -o $@ $^ -s $(LDLIBS)

obj/debug/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/debug/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/release/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/release/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

obj/debug/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/release/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

.PHONY: setup
setup: 
	mkdir -p $(DEPSDIRS) $(BINDIRS) $(OBJDIRS)
	mkdir -p debug/bin release/bin debug/obj release/obj 2> /dev/null

.PHONY: clean
clean:
	rm -f $(OBJS) debug/bin/$(EXEC) release/bin/$(EXEC) $(OUTASM) $(II) $(DEPS)
